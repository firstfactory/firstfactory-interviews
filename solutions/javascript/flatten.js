/*

  Write a function that receives an array of arbitrary nested arrays and returns a flattened array.

  Example:

  Receives [1, [2, 3], 4, [5, [6]], 7]
  Returns [1, 2, 3, 4, 5, 6, 7]

  Rules:

  - Do not use the flat() method

*/

const arr = [1,  [2], [3], 4, [5, [6, [7, [8, [9, 10, [11, [12]]]]]], [13]], 14]

/* Solution with recursion and forEach */
function flattenRecursion(toFlatten = [], flattened = []){
  
  toFlatten.forEach(sub => {
    if(Array.isArray(sub)){
      flattenRecursion(sub, flattened)
    }else{
      flattened.push(sub)
    }
  })

  return flattened
}

console.log("Flatten with recursion: ", flattenRecursion(arr))

/* Solution with recursion and reduce */

function flattenReduce(arr) {
  return arr.reduce(function (flat, toFlatten) {
    return flat.concat(Array.isArray(toFlatten) ? flattenReduce(toFlatten) : toFlatten);
  }, []);
}

console.log("Flatten with reduce: ", flattenReduce(arr))

/* 
  Solution with toString, replace and split.

  While this solution works, we want the ones from above which are more logical
*/

function flattenToString(toFlatten = []){
  const str = toFlatten.toString()
  str.replace("[","");
  str.replace("","]");
  const arr = str.split(",")
  return arr.map(el => Number(el))
}

console.log("Flatten with toString, replace and split: ", flattenToString(arr))



