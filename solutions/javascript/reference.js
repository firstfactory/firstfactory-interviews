/*

  What's the output of the console.logs at the end

*/

function func(person) {
  person.age = 25;
  person = {
      name: 'Carlos',
      age: 50
  };
  
  return person;
}

var person1 = {
  name: 'Jose',
  age: 30
};

var person2 = func(person1);
console.log(person1); // logs { name: 'Jose', age: 25 }
console.log(person2); // logs { name: 'Carlos', age: 50 }