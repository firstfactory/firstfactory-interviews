/*

  Write a function that receives an array of arbitrary nested arrays and returns a flatten array.

  Example:

  Receives [1, [2, 3], 4, [5, [6]], 7]
  Returns [1, 2, 3, 4, 5, 6, 7]

  Rules:

  - Do not use the flat() method

*/

/* Array to be flattened */
const arr = [1,  [2], [3], 4, [5, [6, [7, [8, [9, 10, [11, [12]]]]]], [13]], 14]